package com.ql.dev.customwebview.demo;

import com.ql.dev.customwebview.library.views.HostJsScope;

/**
 * 必须在注入类中定义为public static且第一个参数接收WebView，其他参数的类型可以是int、long、double、boolean、String、JSONObject、JsCallback
 * Created by Administrator on 2018-10-27 0027.
 */
public class CustomHostJsScope extends HostJsScope{


}
