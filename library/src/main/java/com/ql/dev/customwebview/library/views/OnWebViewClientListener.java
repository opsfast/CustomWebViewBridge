package com.ql.dev.customwebview.library.views;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

/**
 *
 * Created by quanguanzhou on 2018-12-07 0007.
 */
public class OnWebViewClientListener {

    public void onPageFinished(WebView view, String url){

    }

    public WebResourceResponse shouldInterceptRequest(WebView view,WebResourceRequest request){
        return null;
    }

    public WebResourceResponse shouldInterceptRequest(WebView view,String request){
        return null;
    }

    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {

    }

    public void onReceivedError(WebView view, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        view.loadUrl("file:///android_asset/error.html");//添加显示本地文件
    }

    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }

    public void shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
    }

    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return false;
    }

    public void onPageStarted(WebView view, Bitmap favicon) {

    }

    public void onLoadResource(WebView view, String url) {

    }

    public void onPageCommitVisible(WebView view, String url) {

    }
}
