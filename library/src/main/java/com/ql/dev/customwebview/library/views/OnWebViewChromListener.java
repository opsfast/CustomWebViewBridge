package com.ql.dev.customwebview.library.views;

import android.graphics.Bitmap;
import android.os.Message;
import android.webkit.WebView;

/**
 * Created by quanguanzhou on 2018-12-06 0006.
 */

public class OnWebViewChromListener {
    /**
     *
     */
    public void onProgressChanged(){}

    public void onReceivedIcon(WebView view, Bitmap icon) {

    }

    public void onReceivedTouchIconUrl(WebView view, String url, boolean precomposed) {

    }

    /**
     *
     * @param view
     * @param isDialog
     * @param isUserGesture
     * @param resultMsg
     * @return
     */
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
        return false;
    }
}
