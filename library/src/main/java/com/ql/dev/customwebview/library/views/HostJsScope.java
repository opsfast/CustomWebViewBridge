package com.ql.dev.customwebview.library.views;

import android.app.Activity;
import android.os.Build;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Toast;

import com.ql.dev.customwebview.library.utils.TaskExecutor;

import java.util.ArrayList;
import java.util.List;


/**
 * 必须在注入类中定义为public static且第一个参数接收WebView，其他参数的类型可以是int、long、double、boolean、String、JSONObject、JsCallback
 * Created by Quanguanzhou on  2017/3/23 0023.
 * Copyright (c) 2015, quanguanzhou83@gmail.com All Rights Reserved.
 */
public class HostJsScope {

    /**
     *
     * @param mWebView
     * @param function 要调用Js的方法：javascript:callJS()
     * @param valueCallback 仅且当只有Build.Version.sdk_int 大于18才有效
     */
    public static void doJavaCallJs(WebView mWebView,String function,ValueCallback<String> valueCallback){
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version <= 18) {
            mWebView.loadUrl(function);
        } else {
            mWebView.evaluateJavascript(function, valueCallback);
        }
    }

    public static void doShowMessage(WebView webView, int msg) {
        doShowMessage(webView, String.valueOf(msg));
    }

    public static void alert(WebView webView, boolean msg) {
        doShowMessage(webView, String.valueOf(msg));
    }

    /**
     * 系统弹出提示框
     * @param webView 必要的参数
     * @param message 提示信息
     * */
    public static void doShowMessage (WebView webView, String message) {
        Toast.makeText((Activity) webView.getContext(),message,Toast.LENGTH_SHORT).show();
    }

    /**
     * Js回调方法
     * @param view 必要的参数
     * @param backMsg
     * @param jsCallback  function (msg) {android.methodName(msg);}
     */
    public static void delayJsCallBack(WebView view,int ms,final String backMsg, final JsCallback jsCallback) {
        TaskExecutor.scheduleTaskOnUiThread(ms * 1000, new Runnable() {
            @Override
            public void run() {
                try {
                    jsCallback.apply(backMsg);
                } catch (JsCallback.JsCallbackException je) {
                    je.printStackTrace();
                }
            }
        });
    }

    public static int overloadMethod(WebView view, int val) {
        return val;
    }

    public static String overloadMethod(WebView view, String val) {
        return val;
    }

    public static class RetJavaObj {
        public int intField;
        public String strField;
        public boolean boolField;
    }

    public static List<RetJavaObj> retJavaObject(WebView view) {
        RetJavaObj obj = new RetJavaObj();
        obj.intField = 1;
        obj.strField = "mine str";
        obj.boolField = true;
        List<RetJavaObj> rets = new ArrayList<RetJavaObj>();
        rets.add(obj);
        return rets;
    }

    public static long passLongType (WebView view, long i) {
        return i;
    }

    public static void toOtherPage(WebView webView,String webSite){
        webView.loadUrl(webSite);
    }

    /**
     * 需要关闭该页面
     * @param view
     */
    public static void doBackPage(WebView view){
        ((Activity)view.getContext()).finish();
    }


}
