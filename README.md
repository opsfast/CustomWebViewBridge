# CustomWebViewBridge

[![Release](https://jitpack.io/v/com.gitee.quanguanzhou/CustomWebViewBridge.svg)](https://jitpack.io/v/com.gitee.quanguanzhou/CustomWebViewBridge.svg)
#### 简介
抛弃使用高风险的WebView addJavascriptInterface方法，通过对js层调用函数及回调函数的包装，支持异步回调，方法参数支持js所有已知的类型，包括number、string、boolean、object、function。
同时还针对WebView的一些常用的方法进行了一定的封装，像返回、刷新、网页中图片保存、是否用系统浏览器进行打开

#### Gitee 地址：[CustomWebViewBridge](https://gitee.com/quanguanzhou/CustomWebViewBridge)

#### 效果图
<img src="./doc/QQ截图20181027140310.png" width="340" height="550" alt="我是缩小后的图"/><img src="./doc/QQ截图20181027140348.png" width="350" height="550" alt="我是缩小后的图"/>
### 用法项目级别 build.gradle：
使用 CustomWebViewBridge 最简单的办法就是像下面这样添加项目依赖。

```xml
    allprojects {
        repositories {
            maven {url'https://jitpack.io' }
        }
    }
```

#### 应用级别 build.gradle
```xml
    dependencies {
        implementation 'com.gitee.quanguanzhou:CustomWebViewBridge:v2.0'
    }
```

#### Maven
```xml
    <!-- <repositories> section of pom.xml -->
    <repository>
        <id>jitpack.io</id>
       <url>https://jitpack.io</url>
    </repository>


    <!-- <dependencies> section of pom.xml -->
    <dependency>
        <groupId>com.gitee.quanguanzhou</groupId>
        <artifactId>CustomWebViewBridge</artifactId>
        <version>v1.0</version>
    </dependency>
```

#### 使用方法

```java
        CustomWebView.Builder builder  = new CustomWebView.Builder(this, (ViewGroup) contentView);
        builder.setPageCenterTitle(centerTitle).setBackColor(backColor);
        builder.setBackPic(backPic).setBackFontColor(backfontColor);
        builder.setCloseFontColor(closeFontColor).setCenterFontColor(centerFontColor);
        builder.setRightFontColor(rightFontColor).setUrl(url);
        builder.setIsShowBack(isShowBack).setIsShowClose(isShowClose).setIsRefresh(isRefresh);
        builder.setNavigationCallbackListener(new OnNavigationCallbackListener() {
            @Override
            public void doLeftbackListener(WebView webView, Activity activity) {
                // 已经处理返回的方法,如果网页中有历史页面可以返回，那就返回历史页面，如果没有就返回到前一Activity
                super.doLeftbackListener(webView, activity);
            }

            @Override
            public void doLeftCloseListener(WebView webView, Activity activity) { //关闭
                super.doLeftCloseListener(webView, activity);
            }

            @Override
            public void doRightCallbackListener(WebView webView, Activity activity) {// 刷新页面
                super.doRightCallbackListener(webView, activity);
            }
        });
        builder.setHostJsScopeClass(CustomHostJsScope.class); // 设置自定义的Js与Java的交互处理类
        builder.create();
```

#### 在Activity或者Fragment的生命周期中与按键返回时添加：
``` java

    /**
     * 监听用户手机按键，如果打开的网址可以返回，
     * 则返回，如果不可以返回则正常处理
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        WebView webView = builder.getWebView();
        if(webView!=null) {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && builder.getWebView().canGoBack()) {
                builder.getWebView().goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        builder.onDestroy();
    }
```

#### 支持的属性有

```java
        Activity activity;
        String isShowBack; //是否显示返回 1是0否

        String isShowClose;//是否显示关闭 1是0否

        String isRefresh;//是否显示刷新 1是0否
        /**
         * 需要显示的Url，可以是：file:///android_asset/、可以是Http、Https网址
         */
        String url;
        /**
         * 返回的图片
         */
        int backPic;
        /**
         * 设置返回的的字体颜色
         */
        int backfontColor;
        /**
         * 设置关闭的字体颜色
         */
        int closeFontColor;
        /**
         * 设置标题的字体颜色
         */
        int centerFontColor;
        /**
         * 设置右边的字体颜色
         */
        int rightFontColor;
        /**
         * 设置右边的文字
         */
        String rightValue;
        /**
         * 设置右边文字左边的图标
         */
        int rightLeftPicResId;
        /**
         * 导航条的背景颜色
         */
        int backColor;
        /**
         * WebView中的连接是否用系统浏览器打开
         * 默认不用系统浏览器打开
         */
        String openBySystem = "0";
        /**
         * 是否启用Js，默认为启用
         */
        boolean javaScriptEnabled = true;
        /**
         * 缓存模式，默认为：WebSettings.LOAD_NO_CACHE;
         */
        int cacheMode = WebSettings.LOAD_NO_CACHE;
        /**
         * 页面标题
         */
        String centerTitle;
        /**
         * CustomWebView，返回、关闭、刷新的自定义点击事件
         */
        OnNavigationCallbackListener onNavigationCallbackListener;
        /**
         * DOM储存
         */
        boolean domStorageEnabled = true;

        Class<? extends HostJsScope> hostJsScopeClass;
```

### 感谢
参考:[(pedant)](https://github.com/pedant/safe-java-js-webview-bridge) 的safe-java-js-webview-bridge库

<img src="./doc/IMG_5514.PNG" width="340" height="450" alt="我是缩小后的图"/>
<img src="./doc/IMG_4845.JPG" width="340" height="400" alt="我是缩小后的图"/><img src="./doc/IMG_4846.JPG" width="350" height="500" alt="我是缩小后的图"/>